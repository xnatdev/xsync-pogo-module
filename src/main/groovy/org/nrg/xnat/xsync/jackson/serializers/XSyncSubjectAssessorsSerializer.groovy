package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import org.nrg.xnat.xsync.config.XSyncSubjectAssessors

class XSyncSubjectAssessorsSerializer extends SyncTypeContainerSerializer<XSyncSubjectAssessors> {

    @Override
    void addAdditionalFields(XSyncSubjectAssessors object, JsonGenerator jsonGenerator) {}

}
