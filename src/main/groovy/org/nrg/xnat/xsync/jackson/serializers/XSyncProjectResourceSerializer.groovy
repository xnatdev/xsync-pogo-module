package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import org.nrg.xnat.xsync.config.XSyncProjectResources

class XSyncProjectResourceSerializer extends SyncTypeContainerSerializer<XSyncProjectResources> {

    @Override
    void addAdditionalFields(XSyncProjectResources object, JsonGenerator jsonGenerator) {}

}
