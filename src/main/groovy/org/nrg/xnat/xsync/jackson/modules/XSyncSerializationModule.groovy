package org.nrg.xnat.xsync.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule
import org.nrg.xnat.xsync.config.XSyncConfig
import org.nrg.xnat.xsync.config.XSyncImagingSessions
import org.nrg.xnat.xsync.config.XSyncProjectResources
import org.nrg.xnat.xsync.config.XSyncSubjectAssessors
import org.nrg.xnat.xsync.config.XSyncSubjectResources
import org.nrg.xnat.xsync.jackson.serializers.*

class XSyncSerializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("XSync_REST_Serializers")

        module.addSerializer(XSyncConfig.class, new XSyncConfigSerializer())
        module.addSerializer(XSyncProjectResources.class, new XSyncProjectResourceSerializer())
        module.addSerializer(XSyncSubjectResources.class, new XSyncSubjectResourceSerializer())
        module.addSerializer(XSyncSubjectAssessors.class, new XSyncSubjectAssessorsSerializer())
        module.addSerializer(XSyncImagingSessions.class, new XSyncImagingSessionsSerializer())

        module
    }

}
