package org.nrg.xnat.xsync.jackson.modules

import com.fasterxml.jackson.databind.module.SimpleModule

class XSyncDeserializationModule {

    static SimpleModule build() {
        final SimpleModule module = new SimpleModule("XSync_REST_Deserializers")

        module
    }

}
