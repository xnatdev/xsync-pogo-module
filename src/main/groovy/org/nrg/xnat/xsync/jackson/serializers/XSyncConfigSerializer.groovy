package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.xsync.config.XSyncConfig

class XSyncConfigSerializer extends CustomSerializer<XSyncConfig> {
    @Override
    void serialize(XSyncConfig xSyncConfig, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject()

        jsonGenerator.writeBooleanField('enabled', xSyncConfig.isEnabled())
        jsonGenerator.writeBooleanField('sync_new_only', xSyncConfig.isSyncNewOnly())
        jsonGenerator.writeBooleanField('anonymize', xSyncConfig.isAnonymize())
        writeStringFieldIfNonnull(jsonGenerator, 'source_project_id', xSyncConfig.getSourceProject().getId())
        writeStringFieldIfNonnull(jsonGenerator, 'remote_url', xSyncConfig.getDestinationXnat())
        writeStringFieldIfNonnull(jsonGenerator, 'remote_project_id', xSyncConfig.getDestinationProject().getId())
        writeEnumToString(jsonGenerator, 'sync_frequency', xSyncConfig.getSyncFrequency().toString())
        writeEnumToString(jsonGenerator, 'identifiers', xSyncConfig.getIdentifiers().toString())
        writeObjectFieldIfNonnull(jsonGenerator, 'project_resources', xSyncConfig.getProjectResources())
        writeObjectFieldIfNonnull(jsonGenerator, 'subject_resources', xSyncConfig.getSubjectResources())
        writeObjectFieldIfNonnull(jsonGenerator, 'subject_assessors', xSyncConfig.getSubjectAssessors())
        writeObjectFieldIfNonnull(jsonGenerator, 'imaging_sessions', xSyncConfig.getImagingSessions())
        writeStringFieldIfNonnull(jsonGenerator, 'notification_emails', xSyncConfig.getNotificationEmails())

        jsonGenerator.writeEndObject()
    }

}
