package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import org.nrg.xnat.xsync.config.XSyncSubjectResources

class XSyncSubjectResourceSerializer extends SyncTypeContainerSerializer<XSyncSubjectResources> {

    @Override
    void addAdditionalFields(XSyncSubjectResources object, JsonGenerator jsonGenerator) {}

}
