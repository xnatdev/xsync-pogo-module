package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import org.nrg.xnat.xsync.config.XSyncImagingSessions

class XSyncImagingSessionsSerializer extends SyncTypeContainerSerializer<XSyncImagingSessions> {

    @Override
    void addAdditionalFields(XSyncImagingSessions object, JsonGenerator jsonGenerator) {}

}
