package org.nrg.xnat.xsync.jackson.serializers

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.nrg.xnat.jackson.serializers.CustomSerializer
import org.nrg.xnat.xsync.config.SyncTypeContainer

abstract class SyncTypeContainerSerializer<T extends SyncTypeContainer> extends CustomSerializer<T> {

    @Override
    void serialize(T object, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject()

        writeEnumToString(jsonGenerator, "sync_type", object.getSyncType())
        writeListFieldIfNonempty(jsonGenerator, "items", object.getItems())
        addAdditionalFields(object, jsonGenerator)

        jsonGenerator.writeEndObject()
    }

    abstract void addAdditionalFields(T object, JsonGenerator jsonGenerator)

}
