package org.nrg.xnat.xsync.results

import org.nrg.xnat.xsync.enums.SyncStatus

class SubjectHistory {

    String localLabel
    SyncStatus syncStatus
    String syncMessage

}
