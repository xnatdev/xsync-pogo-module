package org.nrg.xnat.xsync.results

import org.nrg.xnat.xsync.enums.SyncStatus

class ExperimentHistory {

    String localLabel
    String subjectLabel
    SyncStatus syncStatus
    String syncMessage

}
