package org.nrg.xnat.xsync.results

import org.nrg.xnat.xsync.enums.SyncProcedureType

class XSyncResult {

    int historyId
    boolean syncing
    boolean wasSyncSuccessful
    List<String> completedSubjects
    Map<String, String> completedExperiments
    SyncProcedureType syncType

}
