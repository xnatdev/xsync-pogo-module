package org.nrg.xnat.xsync.results

import org.nrg.xnat.xsync.enums.OverallStatus

class XSyncHistory {

    OverallStatus syncStatus
    int totalSubjects
    int totalExperiments
    int totalAssessors
    List<SubjectHistory> subjectHistories
    List<ExperimentHistory> experimentHistories

}
