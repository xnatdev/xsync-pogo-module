package org.nrg.xnat.xsync.config

import org.nrg.xnat.xsync.enums.SyncType

class XSyncSubjectAssessors extends SyncTypeContainer {

    @Override
    SyncType defaultSyncType() {
        SyncType.NONE
    }

}
