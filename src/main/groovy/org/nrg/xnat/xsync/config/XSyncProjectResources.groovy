package org.nrg.xnat.xsync.config

import org.nrg.xnat.xsync.enums.SyncType

class XSyncProjectResources extends SyncTypeContainer {

    @Override
    SyncType defaultSyncType() {
        SyncType.NONE
    }

}
