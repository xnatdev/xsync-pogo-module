package org.nrg.xnat.xsync.config

import org.nrg.xnat.pogo.AnonScript
import org.nrg.xnat.pogo.Extensible
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.xsync.enums.Identifiers
import org.nrg.xnat.xsync.enums.SyncFrequency

class XSyncConfig extends Extensible<XSyncConfig> {

    boolean enabled = true
    XSyncProjectResources projectResources = new XSyncProjectResources()
    XSyncSubjectResources subjectResources = new XSyncSubjectResources()
    XSyncSubjectAssessors subjectAssessors = new XSyncSubjectAssessors()
    XSyncImagingSessions imagingSessions = new XSyncImagingSessions()
    SyncFrequency syncFrequency = SyncFrequency.WEEKLY
    Identifiers identifiers = Identifiers.USE_LOCAL
    boolean syncNewOnly = true
    boolean anonymize = false
    Project sourceProject
    Project destinationProject
    String destinationXnat
    String notificationEmails = ""
    AnonScript anonScript

    XSyncConfig enabled(boolean enabled) {
        setEnabled(enabled)
        this
    }

    XSyncConfig projectResources(XSyncProjectResources projectResources) {
        setProjectResources(projectResources)
        this
    }

    XSyncConfig subjectResources(XSyncSubjectResources subjectResources) {
        setSubjectResources(subjectResources)
        this
    }

    XSyncConfig subjectAssessors(XSyncSubjectAssessors subjectAssessors) {
        setSubjectAssessors(subjectAssessors)
        this
    }

    XSyncConfig imagingSessions(XSyncImagingSessions imagingSessions) {
        setImagingSessions(imagingSessions)
        this
    }

    XSyncConfig syncFrequency(SyncFrequency syncFrequency) {
        setSyncFrequency(syncFrequency)
        this
    }

    XSyncConfig identifiers(Identifiers identifiers) {
        setIdentifiers(identifiers)
        this
    }

    XSyncConfig syncNewOnly(boolean syncNewOnly) {
        setSyncNewOnly(syncNewOnly)
        this
    }

    XSyncConfig anonymize(boolean anonymize) {
        setAnonymize(anonymize)
        this
    }

    XSyncConfig sourceProject(Project project) {
        setSourceProject(project)
        this
    }

    XSyncConfig destinationProject(Project project) {
        setDestinationProject(project)
        this
    }

    XSyncConfig destinationXnat(String url) {
        setDestinationXnat(url)
        this
    }

    XSyncConfig anonScript(AnonScript anonScript) {
        setAnonScript(anonScript)
        this
    }

}
