package org.nrg.xnat.xsync.config

import org.nrg.xnat.xsync.enums.SyncType

class XSyncImagingSessions extends SyncTypeContainer {

    @Override
    SyncType defaultSyncType() {
        SyncType.ALL
    }

}
