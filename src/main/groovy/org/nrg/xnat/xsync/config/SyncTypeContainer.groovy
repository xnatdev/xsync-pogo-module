package org.nrg.xnat.xsync.config

import org.nrg.xnat.util.ListUtils
import org.nrg.xnat.xsync.enums.SyncType

abstract class SyncTypeContainer {

    private SyncType syncType
    private final List<String> items = []

    abstract SyncType defaultSyncType()

    SyncType getSyncType() {
        syncType ?: defaultSyncType()
    }

    void setSyncType(SyncType syncType) {
        this.syncType = syncType
    }

    SyncTypeContainer syncType(SyncType syncType) {
        setSyncType(syncType)
        this
    }

    List<String> getItems() {
        items
    }

    void setItems(List<String> items) {
        ListUtils.copyInto(items, this.items)
    }

    SyncTypeContainer items(List<String> items) {
        setItems(items)
        this
    }

}
