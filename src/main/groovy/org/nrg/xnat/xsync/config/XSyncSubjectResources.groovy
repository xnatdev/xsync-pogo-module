package org.nrg.xnat.xsync.config

import org.nrg.xnat.xsync.enums.SyncType

class XSyncSubjectResources extends SyncTypeContainer {

    @Override
    SyncType defaultSyncType() {
        SyncType.NONE
    }
    
}
