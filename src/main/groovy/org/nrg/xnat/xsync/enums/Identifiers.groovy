package org.nrg.xnat.xsync.enums;

enum Identifiers {
    USE_LOCAL("use_local")

    private String jsonName

    Identifiers(String jsonName) {
        this.jsonName = jsonName
    }

    @Override
    String toString() {
        jsonName
    }

}
