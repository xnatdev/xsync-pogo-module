package org.nrg.xnat.xsync.enums

enum SyncFrequency {
    ON_DEMAND ("on demand"),
    HOURLY    ("hourly"),
    DAILY     ("daily"),
    WEEKLY    ("weekly"),
    MONTHLY   ("monthly")

    private String jsonName

    SyncFrequency(String jsonName) {
        this.jsonName = jsonName
    }

    @Override
    String toString() {
        jsonName
    }

}
