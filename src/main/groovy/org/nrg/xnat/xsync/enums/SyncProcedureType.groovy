package org.nrg.xnat.xsync.enums

enum SyncProcedureType {
    NONE_SINCE_STARTUP ("NONE_SINCE_STARTUP"),
    PROJECT_SYNC       ("PROJECT_SYNC"),
    EXPERIMENT_SYNC    ("EXPERIMENT_SYNC")

    private String jsonName

    SyncProcedureType(String jsonName) {
        this.jsonName = jsonName
    }

    @Override
    String toString() {
        jsonName
    }
}