package org.nrg.xnat.xsync.enums

enum OverallStatus {
    COMPLETE_AND_VERIFIED ("Complete [Verified]"),
    COMPLETE_NOT_VERIFIED ("Complete [NOT Verified]"),
    COMPLETE              ("Complete"),
    FAIL                  ("Fail")

    private String jsonName

    OverallStatus(String jsonName) {
        this.jsonName = jsonName
    }

    String getDisplayName() {
        return jsonName
    }

    @Override
    String toString() {
        jsonName
    }
}
