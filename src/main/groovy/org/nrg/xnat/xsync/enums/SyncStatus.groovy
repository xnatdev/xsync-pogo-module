package org.nrg.xnat.xsync.enums

enum SyncStatus {
    SYNCED_AND_VERIFIED ("SYNCED_AND_VERIFIED"),
    SYNCED_NOT_VERIFIED ("SYNCED_AND_NOT_VERIFIED"),
    SKIPPED             ("SKIPPED")

    private String jsonName

    SyncStatus(String displayName) {
        this.jsonName = displayName
    }

    @Override
    String toString() {
        jsonName
    }

}
