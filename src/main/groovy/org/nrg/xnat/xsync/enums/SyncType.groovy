package org.nrg.xnat.xsync.enums

enum SyncType {
    ALL     ("all"),
    NONE    ("none"),
    INCLUDE ("include"),
    EXCLUDE ("exclude")

    private String jsonName

    SyncType(String jsonName) {
        this.jsonName = jsonName
    }

    @Override
    String toString() {
        return jsonName
    }

}
